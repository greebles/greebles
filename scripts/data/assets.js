var assets = {
    textures: [{
            handle: "LOADING",
            path: "img/loading.png",
            color: [1, 1, 1, 1],
            textureRectangle: [0, 0, 128, 256],
            width: 128,
            height: 256,
            x: 320,
            y: 240,
            origin: [64, 128]
        },
        // {
        //     handle: "BORDER_TOP",
        //     path: "img/border-top.png",
        //     textureRectangle: [0, 0, 800, 60],
        //     width: 800,
        //     height: 60,
        //     x: 0,
        //     y: 0,
        //     origin: [0, 0]
        // },
        // {
        //     handle: "BORDER_BOTTOM",
        //     path: "img/border-bottom.png",
        //     textureRectangle: [0, 0, 800, 60],
        //     width: 800,
        //     height: 60,
        //     x: 0,
        //     y: 540,
        //     origin: [0, 0]
        // },
        // {
        //     handle: "BORDER_LEFT",
        //     path: "img/border-left.png",
        //     textureRectangle: [0, 0, 80, 480],
        //     width: 80,
        //     height: 480,
        //     x: 0,
        //     y: 60,
        //     origin: [0, 0]
        // },
        // {
        //     handle: "BORDER_RIGHT",
        //     path: "img/border-right.png",
        //     textureRectangle: [0, 0, 80, 480],
        //     width: 80,
        //     height: 480,
        //     x: 720,
        //     y: 60,
        //     origin: [0, 0]
        // },
        {
            handle: "BLOCKS",
            path: "img/blocks.png",
            textureRectangle: [0, 0, 768, 144],
            width: 768,
            height: 144,
            origin: [0, 0]
        }, {
            handle: "NUMBERS",
            path: "img/numbers.png",
            textureRectangle: [0, 0, 80, 99],
            width: 80,
            height: 99,
            origin: [0, 0]
        }, {
            handle: "PATTERNS",
            path: "img/patterns.png",
            origin: [0, 0]
        }, {
            handle: "PLAYERS",
            path: "img/players.png",
            textureRectangle: [0, 0, 816, 864],
            width: 816,
            height: 864,
            origin: [0, 0]
        }, {
            handle: "PERCENT_BAR_BACKGROUND",
            path: "img/percentage-bar-background.png",
            textureRectangle: [0, 0, 87, 16],
            width: 87,
            height: 16,
            origin: [0, 0]
        }, {
            handle: "PERCENT_BAR_FOREGROUND",
            path: "img/percentage-bar-foreground.png",
            textureRectangle: [0, 0, 87, 16],
            width: 87,
            height: 16,
            origin: [0, 0]
        }, {
            handle: "TITLE",
            path: "screen/title.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "TITLE_DIMMED",
            path: "screen/title-dimmed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "TITLE_HOVER",
            path: "screen/title-hover.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "TITLE_PRESSED",
            path: "screen/title-pressed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "THANKS",
            path: "screen/thanks.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "THANKS_HOVER",
            path: "screen/thanks-hover.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "THANKS_PRESSED",
            path: "screen/thanks-pressed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "PAUSE",
            path: "screen/pause.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "PAUSE_HOVER",
            path: "screen/pause-hover.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "PAUSE_PRESSED",
            path: "screen/pause-pressed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "NET",
            path: "screen/net.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "NET_HOVER",
            path: "screen/net-hover.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "NET_PRESSED",
            path: "screen/net-pressed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "INST_1",
            path: "screen/instructions-1.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "INST_1_HOVER",
            path: "screen/instructions-1-hover.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "INST_1_PRESSED",
            path: "screen/instructions-1-pressed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "INST_2",
            path: "screen/instructions-2.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "INST_2_HOVER",
            path: "screen/instructions-2-hover.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }, {
            handle: "INST_2_PRESSED",
            path: "screen/instructions-2-pressed.png",
            textureRectangle: [0, 0, 640, 480],
            width: 640,
            height: 480,
            origin: [0, 0]
        }
    ],
    fonts: [{
        handle: "hero",
        path: "font/hero.fnt"
    }],
    shaders: [{
        handle: "hero",
        path: "shader/font.cgfx"
    }]
};