/*global MultiplayerWaitingState: false*/
/*global GameplayDescriptionState: false*/
/*global ItemDescriptionState: false*/
/*global MainMenuState: false*/

MainMenuState.data = {
    textureNames: {
        idle: "TITLE",
        hover: "TITLE_HOVER",
        pressed: "TITLE_PRESSED"
    },
    buttons: [{
        x: 580,
        y: 0,
        width: 60,
        height: 30,
        pressed: "First"
    }, {
        x: 390,
        y: 370,
        width: 240,
        height: 55,
        pressed: "Second"
    }, {
        x: 400,
        y: 430,
        width: 240,
        height: 50,
        pressed: "Third"
    }, {
        x: 64,
        y: 100,
        width: 280,
        height: 25,
        pressed: MainMenuState.playClicked.bind(MainMenuState)
    }, {
        x: 64,
        y: 134,
        width: 280,
        height: 25,
        pressed: "Fifth"
    }, {
        x: 64,
        y: 168,
        width: 280,
        height: 25,
        pressed: MainMenuState.hostNetworkGameClicked.bind(MainMenuState)
    }, {
        x: 64,
        y: 202,
        width: 280,
        height: 25,
        pressed: MainMenuState.joinNetworkGameClicked.bind(MainMenuState)
    }, {
        x: 64,
        y: 236,
        width: 280,
        height: 25,
        pressed: "Eigth"
    }, {
        x: 64,
        y: 270,
        width: 280,
        height: 25,
        pressed: MainMenuState.instructionsClicked.bind(MainMenuState)
    }, {
        x: 64,
        y: 304,
        width: 280,
        height: 25,
        pressed: "Tenth"
    }, {
        x: 64,
        y: 338,
        width: 280,
        height: 25,
        pressed: "Eleventh"
    }, {
        x: 64,
        y: 372,
        width: 280,
        height: 25,
        pressed: "Twelfth"
    }, {
        x: 64,
        y: 406,
        width: 280,
        height: 25,
        pressed: MainMenuState.quitClicked
    }]
};

MultiplayerWaitingState.data = {
    textureNames: {
        idle: "NET",
        hover: "NET_HOVER",
        pressed: "NET_PRESSED"
    },
    buttons: [{
        x: 36,
        y: 356,
        width: 160,
        height: 110,
        pressed: MultiplayerWaitingState.cancelClicked.bind(MultiplayerWaitingState)
    }, {
        x: 444,
        y: 356,
        width: 160,
        height: 110,
        pressed: MultiplayerWaitingState.joinClicked.bind(MultiplayerWaitingState)
    }]
};

ItemDescriptionState.data = {
    textureNames: {
        idle: "INST_1",
        hover: "INST_1_HOVER",
        pressed: "INST_1_PRESSED"
    },
    text: "",
    textX: 185,
    textY: 190,
    buttons: [{
        x: 99,
        y: 82,
        width: 25,
        height: 25,
        hover: "You can have up to four players on\neach machine and up to nine machines\n(36 players total!). Finally, a\nmulti-player game you can play with or\nwithout a network!",
    }, {
        x: 127,
        y: 82,
        width: 25,
        height: 25,
        hover: "Human players can play together or in\ncompetition. Either way the game\ndoesn't mind: players can switch at any\ntime. At least Computer players stay\nFriendly or Nasty!",
    }, {
        x: 154,
        y: 82,
        width: 25,
        height: 25,
        hover: "Each player starts in their own corner.\nTry to ensure you are evenly spread\nacross all the corners - starting in a\ncorner with someone better than you is\na crushing experience.",
    }, {
        x: 181,
        y: 82,
        width: 25,
        height: 25,
        hover: "You get a small head start on the\nGreebles at the beginning of each level\n- don't waste it! Before the Greebles\nemerge fully from the blocks they are\nvulnerable and can be crushed.",
    }, {
        x: 99,
        y: 113,
        width: 25,
        height: 25,
        hover: "Fly - This is your basic Greeble; it\nwanders around pretty much randomly.\nIt's not very fast (at least at early\nlevels) and it tends not to reverse directions.",
    }, {
        x: 128,
        y: 113,
        width: 22,
        height: 25,
        hover: "Crab - Hardly more dangerous than the\nbasic Fly. Still, if you stand around he\ncan still kill you.",
    }, {
        x: 154,
        y: 113,
        width: 25,
        height: 25,
        hover: "Dump Truck - Usually dump trucks don't\ngo quite as fast as this guy. As with\nother dump trucks, it's a good idea to\nstay out of its way - either that or\nyou'll be flattened like a pancake.",
    }, {
        x: 181,
        y: 113,
        width: 25,
        height: 25,
        hover: "Cockroach - These guys scurry about\ncausing a nuisance of themselves. I\n don't know about you, but they give me\nthe creeps! Kill, kill, kill - die you evil\nbugs!",
    }, {
        x: 209,
        y: 132,
        width: 16,
        height: 6,
        hover: "MSG_GREEBLE_ELLIPSIS_HOVER",
    }, {
        x: 99,
        y: 145,
        width: 25,
        height: 25,
        hover: "Blocks can be pushed or crushed (push\na block against another block).\nCrushing a block captures the special it\ncontains, although not all specials can\nbe pushed or crushed.",
    }, {
        x: 127,
        y: 145,
        width: 25,
        height: 25,
        hover: "In Easy mode, blocks won't squish you.\nBut in the other modes beware of the\nbounce! You can safely escape so long\nas there is more than a one block\nspace.",
    }, {
        x: 154,
        y: 145,
        width: 25,
        height: 25,
        hover: "Blocks bounce twice before coming to a\nhalt, unless they hit a bounce block. Or\nthey bounce off a moving player or\nblock, or you are in Suicide mode, or...",
    }, {
        x: 181,
        y: 145,
        width: 25,
        height: 25,
        hover: "At higher levels Greeble generators\nhave to be surrounded on all four sides\nby blocks: they can't be crushed. This\ncan be tricky, but it is always possible.",
    }, {
        x: 209,
        y: 164,
        width: 16,
        height: 6,
        hover: "MSG_BLOCK_ELLIPSIS_HOVER",
    }, {
        x: 287,
        y: 89,
        width: 132,
        height: 39,
        hover: "Escape - Press the escape key during\nthe game to pause, kill the game,\nchange your keys, read the instructions\nor hide Greebles.",
    }, {
        x: 423,
        y: 88,
        width: 190,
        height: 59,
        hover: "Press the number keys at the top of\nthe keyboard to change the volume\nlevel.",
    }, {
        x: 11,
        y: 183,
        width: 153,
        height: 105,
        hover: "More - Click this to get a more detailed\nlook at the mechanics of the game.",
        pressed: ItemDescriptionState.moreClicked.bind(ItemDescriptionState)
    }, {
        x: 475,
        y: 183,
        width: 153,
        height: 105,
        hover: "Done - Click this to go back to the main\nscreen. You can also press <return> -\nmost buttons have a key equivalent,\njust type randomly and see what\nhappens.",
        pressed: ItemDescriptionState.doneClicked.bind(ItemDescriptionState)
    }, {
        x: 40,
        y: 322,
        width: 86,
        height: 25,
        hover: "Bonus - gives you points, good for a\nhigh score but not much else (and you'll\nlose your score if you die on the level,\nso don't worry about these guys too\nmuch).",
    }, {
        x: 40,
        y: 348,
        width: 87,
        height: 25,
        hover: "Shield - gives you super strength,\nblocks bounce off you and Greebles\ncan't hurt you. Useful for sculpting the\nmaze. Watch out, it only lasts for a\nshort time!",
    }, {
        x: 40,
        y: 374,
        width: 87,
        height: 25,
        hover: "Speed - gives you super speed, out run\n(most) Greebles and blocks. Use this to\nrace around killing Greebles and\ngrabbing specials. But after a short\ntime you'll feel like molasses!",
    }, {
        x: 40,
        y: 400,
        width: 91,
        height: 25,
        hover: "Freeze - If everyone else is moving too\nfast for you, grab one of these. For ten\nseconds you get to kill everything on\ntouch. Grabbing another one before\nthis one runs out is a good idea, too!",
    }, {
        x: 40,
        y: 426,
        width: 98,
        height: 25,
        hover: "Fireball - Now is the time to get even!\nThrow exploding fireballs in all\ndirections. Be careful or you'll kill\nyourself faster than you can say \"Die\nScum!\".",
    }, {
        x: 40,
        y: 452,
        width: 141,
        height: 25,
        hover: "Death Touch - Our turn to do some\nwiping out! Whoosh with our heat ray!\nAnd them running and dying, beaten at\ntheir own game. This one is fun!\nKill'em all, kill'em fast!",
    }, {
        x: 220,
        y: 322,
        width: 145,
        height: 25,
        hover: "Unbreakable - Don't bother trying to\ncrush these guys, your puny strength is\nno match for their reinforced structure.\nAt least you can throw them around.",
    }, {
        x: 220,
        y: 348,
        width: 129,
        height: 25,
        hover: "Immovable - These are nailed down.\nDon't bother trying to push them. And\nif your figure you can just crush them -\nthink again! They are here to stay.\nOnly natural decay can remove them.",
    }, {
        x: 220,
        y: 374,
        width: 100,
        height: 25,
        hover: "Line-Up - Those cute little Stairways\nicons can come in mighty handy, if you\ncan get enough of them together in one\nplace they'll break open and give you\nsome goodies. Don't crush them!",
    }, {
        x: 220,
        y: 400,
        width: 107,
        height: 25,
        hover: "Grenade - These guys don't bounce like\nregular blocks, but they do explode.\nGreat for trick shots like \"round the\ncorner\" and \"through the block\".",
    }, {
        x: 220,
        y: 426,
        width: 179,
        height: 25,
        hover: "Bounce Grenade - Like grenades, but\nharder to use. They are a bit tougher\nand bounce a few times before\nexploding. Still, with some careful\ntiming (or lots of luck)...",
    }, {
        x: 220,
        y: 452,
        width: 99,
        height: 25,
        hover: "Bounce - Painful, that's what they are.\nBricks that bounce off these guys don't\nslow down at all. And if there is one on\neach side, well, can you say \"killing\nzone\"? Be careful!",
    }, {
        x: 400,
        y: 322,
        width: 196,
        height: 25,
        hover: "Greeble Generator - These periodically\nproduce a new Greeble. Usually you\ncan just crush them, but later on you\nhave to surround them with blocks\n(tricky!). Listen for the warnings!",
    }, {
        x: 400,
        y: 348,
        width: 163,
        height: 25,
        hover: "Timed Release - This one turns into a\nrandom special. Just hope it doesn't\nturn into a Greeble Generator! Keep\nan eye on these and hope for the best.",
    }, {
        x: 400,
        y: 374,
        width: 133,
        height: 25,
        hover: "Skip Levels - If it's all to easy, crush\none of these and you'll skip forward\nfive levels (assuming you manage to\nfinish the one you're on of course!).",
    }, {
        x: 400,
        y: 400,
        width: 118,
        height: 25,
        hover: "Extra Life - Collect these! You'll need\nlives to get past some of those tricky\nlater levels. Unless you're really good,\nthen you can spurn them and try to get\nthrough without losing a life.",
    }]
};

GameplayDescriptionState.data = {
    textureNames: {
        idle: "INST_2",
        hover: "INST_2_HOVER",
        pressed: "INST_2_PRESSED"
    },
    text: "",
    textX: 55,
    textY: 285,
    buttons: [{
        x: 0,
        y: 0,
        width: 86,
        height: 57,
        hover: "Special Name - Shows the last special a player\ncrushed - if you can't remember what a\nspecial icon means, this will tell you."
    }, {
        x: 148,
        y: 0,
        width: 259,
        height: 55,
        hover: "Level Title - This gives the absolute level\nnumber and the name of the level. The name\nis often a hint about the level!"
    }, {
        x: 520,
        y: 0,
        width: 120,
        height: 233,
        hover: "Player Information - The player's name,\ncurrent score, the number of lives remaining\nand what specials they have. Xes indicate\nthe player is dead."
    }, {
        x: 507,
        y: 396,
        width: 133,
        height: 84,
        hover: "Greeble Value Percentage - This percentage\ncounts down from 100% to 0%. When a\nGreeble is killed it is worth only the current\npercentage of its initial value."
    }, {
        x: 216,
        y: 90,
        width: 210,
        height: 85,
        hover: "Push a block - The blue player has just pushed\non a block. This time he is squishing a\nGreeble. Next time it could be a player. The\ngame doesn't mind so it's up to you!"
    }, {
        x: 57,
        y: 151,
        width: 98,
        height: 98,
        hover: "Crush a block - The purple player has just\ngained a special by crushing a block. The\nplayer pushed on the block which can't move,\nso the block is crushed, releasing the special."
    }, {
        x: 18,
        y: 406,
        width: 181,
        height: 79,
        hover: "Squish! - The purple player just squished two\nGreebles gaining 360 points for the first and\ntwo times 360 points for the second. (A third\nwould be worth 1440 points!)"
    }, {
        x: 404,
        y: 275,
        width: 126,
        height: 86,
        hover: "Done - Takes you back to the first instructions\npage.",
        pressed: GameplayDescriptionState.doneClicked.bind(GameplayDescriptionState)
    }]
};
