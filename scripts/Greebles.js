/**
 * Greebles.js
 * Main Source file for Greebles!
 */

/*global console: false */

// Turbulenz Things
/*global Draw2D: false */
/*global Draw2DSprite: false */
/*global FontManager: false */
/*global RequestHandler: false */
/*global ShaderManager: false */
/*global TextureManager: false */
/*global TurbulenzEngine: false */
/*global TurbulenzServices: false */

// External Libs
/*global async: false */

// Greebles Resources
/*global assets: false */
/*global MainMenuState: false */

//  __       __                                         
// |  \     /  \                                        
// | $$\   /  $$  ______   __    __   _______   ______  
// | $$$\ /  $$$ /      \ |  \  |  \ /       \ /      \ 
// | $$$$\  $$$$|  $$$$$$\| $$  | $$|  $$$$$$$|  $$$$$$\
// | $$\$$ $$ $$| $$  | $$| $$  | $$ \$$    \ | $$    $$
// | $$ \$$$| $$| $$__/ $$| $$__/ $$ _\$$$$$$\| $$$$$$$$
// | $$  \$ | $$ \$$    $$ \$$    $$|       $$ \$$     \
//  \$$      \$$  \$$$$$$   \$$$$$$  \$$$$$$$   \$$$$$$$
var Mouse = (function() {
    function Mouse(context) {
        var self = this,
            inputDevice = context.devices.inputDevice;

        this.mouseCodes = inputDevice.mouseCodes;
        this.position = {
            x: 0,
            y: 0
        };

        this.buttons = {};
        Object.keys(this.mouseCodes).forEach(function(code) {
            self.buttons[self.mouseCodes[code]] = {
                state: false,
                lock: false
            };
        });

        inputDevice.addEventListener('mouseover', function(x, y) {
            self.position.x = x;
            self.position.y = y;
        });

        inputDevice.addEventListener('mousedown', function(mouseCode, x, y) {
            self.position.x = x;
            self.position.y = y;

            self.buttons[mouseCode].state = true;
        });

        inputDevice.addEventListener('mouseup', function(mouseCode, x, y) {
            self.position.x = x;
            self.position.y = y;

            self.buttons[mouseCode].state = false;
            self.buttons[mouseCode].lock = false;
        });
    }

    Mouse.prototype.buttonClicked = function(mouseCode) {
        var buttons = this.buttons;

        if (buttons[mouseCode].state && !buttons[mouseCode].lock) {
            buttons[mouseCode].lock = true;
            return true;
        }

        return false;
    };

    Mouse.prototype.buttonDown = function(mouseCode) {
        var buttons = this.buttons;

        return buttons[mouseCode].state;
    };

    Mouse.prototype.buttonUp = function(mouseCode) {
        return !this.buttonDown(mouseCode);
    };

    Mouse.create = function(context) {
        return new Mouse(context);
    };

    return Mouse;
})();


//  __    __                     __                                            __ 
// |  \  /  \                   |  \                                          |  \
// | $$ /  $$ ______   __    __ | $$____    ______    ______    ______    ____| $$
// | $$/  $$ /      \ |  \  |  \| $$    \  /      \  |      \  /      \  /      $$
// | $$  $$ |  $$$$$$\| $$  | $$| $$$$$$$\|  $$$$$$\  \$$$$$$\|  $$$$$$\|  $$$$$$$
// | $$$$$\ | $$    $$| $$  | $$| $$  | $$| $$  | $$ /      $$| $$   \$$| $$  | $$
// | $$ \$$\| $$$$$$$$| $$__/ $$| $$__/ $$| $$__/ $$|  $$$$$$$| $$      | $$__| $$
// | $$  \$$\\$$     \ \$$    $$| $$    $$ \$$    $$ \$$    $$| $$       \$$    $$
//  \$$   \$$ \$$$$$$$ _\$$$$$$$ \$$$$$$$   \$$$$$$   \$$$$$$$ \$$        \$$$$$$$
//                    |  \__| $$                                                  
//                     \$$    $$                                                  
//                      \$$$$$$                                                   
var Keyboard = (function() {
    function Keyboard(context) {
        var self = this,
            inputDevice = context.devices.inputDevice;

        this.keyCodes = context.devices.inputDevice.keyCodes;
        this.keys = {};
        Object.keys(this.keyCodes).forEach(function(key) {
            self.keys[self.keyCodes[key]] = {
                state: false,
                lock: false
            };
        });

        inputDevice.addEventListener('keydown', function(keyCode) {
            self.keys[keyCode].state = true;
        });

        inputDevice.addEventListener('keyup', function(keyCode) {
            self.keys[keyCode].state = false;
            self.keys[keyCode].lock = false;
        });
    }

    Keyboard.prototype.keyHit = function(keyCode) {
        var key = this.keys[keyCode];

        if (key.state && !key.lock) {
            key.lock = true;
            return true;
        }

        return false;
    };

    Keyboard.prototype.keyDown = function(keyCode) {
        return this.keys[keyCode].state;
    };

    Keyboard.create = function(context) {
        return new Keyboard(context);
    };

    return Keyboard;
})();


//   ______     __                 __                       ______     __                          __       
//  /      \   |  \               |  \                     /      \   |  \                        |  \      
// |  $$$$$$\ _| $$_     ______  _| $$_     ______        |  $$$$$$\ _| $$_     ______    _______ | $$   __ 
// | $$___\$$|   $$ \   |      \|   $$ \   /      \       | $$___\$$|   $$ \   |      \  /       \| $$  /  \
//  \$$    \  \$$$$$$    \$$$$$$\\$$$$$$  |  $$$$$$\       \$$    \  \$$$$$$    \$$$$$$\|  $$$$$$$| $$_/  $$
//  _\$$$$$$\  | $$ __  /      $$ | $$ __ | $$    $$       _\$$$$$$\  | $$ __  /      $$| $$      | $$   $$ 
// |  \__| $$  | $$|  \|  $$$$$$$ | $$|  \| $$$$$$$$      |  \__| $$  | $$|  \|  $$$$$$$| $$_____ | $$$$$$\ 
//  \$$    $$   \$$  $$ \$$    $$  \$$  $$ \$$     \       \$$    $$   \$$  $$ \$$    $$ \$$     \| $$  \$$\
//   \$$$$$$     \$$$$   \$$$$$$$   \$$$$   \$$$$$$$        \$$$$$$     \$$$$   \$$$$$$$  \$$$$$$$ \$$   \$$
var StateStack = (function() {
    function StateStack(context) {
        this.context = context;
        this.states = [];
    }

    StateStack.prototype.push = function(newState) {
        this.validateState(newState);

        this.states.push(newState);
        this.getCurrentState().enter();
    };

    StateStack.prototype.pop = function() {
        var states = this.states;

        if (states.length <= 0) {
            return;
        }

        this.getCurrentState().exit();
        states.pop();
    };

    StateStack.prototype.getCurrentState = function() {
        return this.states[this.states.length - 1];
    };

    StateStack.prototype.validateState = function(state) {
        if (!state.enter || !state.update || !state.render || !state.exit) {
            console.log("State missing needed functions.");
            console.log(state);
        }

        if (!state.context) {
            state.context = this.context;
        }
    };

    StateStack.create = function(context) {
        return new StateStack(context);
    };

    return StateStack;
})();


//   ______                                    
//  /      \                                   
// |  $$$$$$\  ______   ______ ____    ______  
// | $$ __\$$ |      \ |      \    \  /      \ 
// | $$|    \  \$$$$$$\| $$$$$$\$$$$\|  $$$$$$\
// | $$ \$$$$ /      $$| $$ | $$ | $$| $$    $$
// | $$__| $$|  $$$$$$$| $$ | $$ | $$| $$$$$$$$
//  \$$    $$ \$$    $$| $$ | $$ | $$ \$$     \
//   \$$$$$$   \$$$$$$$ \$$  \$$  \$$  \$$$$$$$
var Game = (function() {
    function Game(context) {
        this.context = context;
        this.ss = StateStack.create(context);
    }

    Game.prototype.init = function() {
        this.ss.push(MainMenuState);
    };

    Game.prototype.update = function() {
        var currentState = this.ss.getCurrentState();

        if (!currentState) {
            return;
        }

        currentState.update();
    };

    Game.prototype.render = function() {
        var currentState = this.ss.getCurrentState();

        if (!currentState) {
            return;
        }

        currentState.render();
    };

    Game.create = function(context) {
        return new Game(context);
    };

    return Game;
})();


//   ______                                 __        __                     
//  /      \                               |  \      |  \                    
// |  $$$$$$\  ______    ______    ______  | $$____  | $$  ______    _______ 
// | $$ __\$$ /      \  /      \  /      \ | $$    \ | $$ /      \  /       \
// | $$|    \|  $$$$$$\|  $$$$$$\|  $$$$$$\| $$$$$$$\| $$|  $$$$$$\|  $$$$$$$
// | $$ \$$$$| $$   \$$| $$    $$| $$    $$| $$  | $$| $$| $$    $$ \$$    \ 
// | $$__| $$| $$      | $$$$$$$$| $$$$$$$$| $$__/ $$| $$| $$$$$$$$ _\$$$$$$\
//  \$$    $$| $$       \$$     \ \$$     \| $$    $$| $$ \$$     \|       $$
//   \$$$$$$  \$$        \$$$$$$$  \$$$$$$$ \$$$$$$$  \$$  \$$$$$$$ \$$$$$$$ 
var Greebles = (function() {

    function Greebles() {
        this.devices = {};
        this.managers = {};
        this.sprites = [];

        this.fontTechnique = null;
        this.fontTechniqueParams = null;

        this.gameSettings = {
            assetCount: assets.textures.length + assets.fonts.length + assets.shaders.length,
            fps: 1000 / 60,
            screen: {
                width: 800,
                height: 600,
                clearColor: [1.0, 1.0, 1.0, 1.0]
            },
            currentLevel: 1
        };

        this.debug = {
            fps: {
                domElement: document.getElementById("fps"),
                last: "",
                nextUpdate: 0
            }
        };
    }

    Greebles.prototype.errorCallback = function(msg) {
        window.alert("Greebles: " + msg);
    };

    Greebles.prototype.createDevices = function() {
        var devices = this.devices,
            managers = this.managers,
            errorCallback = this.errorCallback;

        var graphicsDeviceParameters = { multisample: 16 };
        var graphicsDevice = TurbulenzEngine.createGraphicsDevice(graphicsDeviceParameters);

        var mathDeviceParameters = {};
        var mathDevice = TurbulenzEngine.createMathDevice(mathDeviceParameters);

        var inputDeviceParameters = {};
        var inputDevice = TurbulenzEngine.createInputDevice(inputDeviceParameters);

        var networkDeviceParameters = {};
        var networkDevice = TurbulenzEngine.createNetworkDevice(networkDeviceParameters);

        devices.graphicsDevice = graphicsDevice;
        devices.mathDevice = mathDevice;
        
        devices.inputDevice = inputDevice;
        devices.mouse = Mouse.create(this);
        devices.keyboard = Keyboard.create(this);
        
        devices.networkDevice = networkDevice;
        
        var draw2dParameters = { graphicsDevice: graphicsDevice };
        var draw2d = Draw2D.create(draw2dParameters);

        this.draw2d = draw2d;

        var requestHandlerParameters = {};
        var requestHandler = RequestHandler.create(requestHandlerParameters);
        this.requestHandler = requestHandler;

        managers.textureManager = TextureManager.create(graphicsDevice, requestHandler, null, errorCallback);
        managers.shaderManager = ShaderManager.create(graphicsDevice, requestHandler, null, errorCallback);
        managers.fontManager = FontManager.create(graphicsDevice, requestHandler, null, errorCallback);

        return true;
    };

    Greebles.prototype.init = function() {
        if (!this.createDevices()) {
            return;
        }

        var creationFunctions = [
            { func: this.createGameSession, isDependent: false },
            { func: this.createMappingTable, isDependent: true },
            { func: this.createMultiplayerSessionManager, isDependent: false, noCallback: true },
            { func: this.createGame, isDependent: true, noCallback: true },
            { func: this.enterLoadingLoop, isDependent: true }
        ];

        this.enterCallbackChain(this, creationFunctions);
    };

    Greebles.prototype.createGameSession = function(callback) {
        var requestHandler = this.requestHandler;

        this.gameSession = TurbulenzServices.createGameSession(requestHandler, callback);
    };

    Greebles.prototype.createMappingTable = function (callback) {
        this.mappingTable = TurbulenzServices.createMappingTable(this.requestHandler, this.gameSession, callback);
    };

    Greebles.prototype.createMultiplayerSessionManager = function (callback) {
        this.multiplayerSessionManager = TurbulenzServices.createMultiplayerSessionManager(this.requestHandler, this.gameSession);
    };

    Greebles.prototype.createGame = function() {
        this.game = Game.create(this);
        this.game.init();
    };

    Greebles.prototype.enterLoadingLoop = function() {
        var managers = this.managers,
            mappingTable = this.mappingTable;

        var urlMapping = mappingTable.urlMapping,
            assetPrefix = '';

        managers.textureManager.setPathRemapping(urlMapping, assetPrefix);
        managers.fontManager.setPathRemapping(urlMapping, assetPrefix);
        managers.shaderManager.setPathRemapping(urlMapping, assetPrefix);

        this.loadSprites(mappingTable);
        this.loadFonts(mappingTable);
        this.loadShaders(mappingTable);

        this.intervalId = TurbulenzEngine.setInterval(this.loadingLoop.bind(this), this.gameSettings.fps);
    };

    Greebles.prototype.loadSprites = function(mappingTable) {
        var textureManager = this.managers.textureManager,
            sprites = this.sprites;

        function onLoad(texture, textureInstance) {
            texture.texture = textureInstance.getTexture();
            sprites[texture.handle] = Draw2DSprite.create(texture);
        }

        function loadTexture(texture) {
            textureManager.load(mappingTable.getURL(texture.path), false, onLoad.bind(this, texture));
        }

        assets.textures.forEach(loadTexture);
    };

    Greebles.prototype.loadFonts = function() {
        var fontManager = this.managers.fontManager;

        function loadFont(font) {
            fontManager.load(font.path);
        }

        assets.fonts.forEach(loadFont);
    };

    Greebles.prototype.loadShaders = function() {
        var shaderManager = this.managers.shaderManager;

        function loadShader(shader) {
            shaderManager.load(shader.path);
        }

        assets.shaders.forEach(loadShader);
    };

    Greebles.prototype.enterCallbackChain = function(context, functions) {
        var length = functions.length,
            localCallback,
            callNextFunction;

        // Invariant: currentFunction always refers to the last uncalled function
        var currentFunction = 0;

        // Invariant: activeCallbacks refers to the number of functions whose callbacks have not yet been received
        var activeCallbacks = 0;

        callNextFunction = function callNextFunctionFn() {
            if (!functions[currentFunction].noCallback) {
                activeCallbacks += 1;
            }

            functions[currentFunction].func.call(context, localCallback, arguments);

            currentFunction += 1;
        };

        localCallback = function localCallbackFn() {
            activeCallbacks -= 1;

            if (activeCallbacks === 0 && currentFunction < length) {
                // No active callbacks so immediately call next function
                callNextFunction();

                while (currentFunction < length && ((0 === activeCallbacks) || (!functions[currentFunction].isDependent))) {
                    callNextFunction();
                }
            }
        };

        // Start the async callback chain
        callNextFunction();
    };

    Greebles.prototype.loadingLoop = function() {
        var graphicsDevice = this.devices.graphicsDevice,
            managers = this.managers,
            clearColor = this.gameSettings.screen.clearColor,
            totalAssetCount = this.gameSettings.assetCount,
            draw2d = this.draw2d,
            sprites = this.sprites;

        if (this.allAssetsLoaded()) {
            this.helpFont = managers.fontManager.get('font/hero.fnt');
            this.helpFontShader = managers.shaderManager.get('shader/font.cgfx');
            this.helpFontShader.fontTechnique = this.helpFontShader.getTechnique('font');

            TurbulenzEngine.clearInterval(this.intervalId);
            this.intervalId = TurbulenzEngine.setInterval(this.update.bind(this), this.gameSettings.fps);
            return;
        }

        if (!graphicsDevice.beginFrame() || !sprites["LOADING"]) {
            return;
        }

        graphicsDevice.clear(clearColor, 1.0, 0.0);

        this.adjustView();

        draw2d.begin('alpha', 'texture');
        draw2d.drawSprite(sprites["LOADING"]);
        draw2d.end();

        graphicsDevice.endFrame();

    };

    Greebles.prototype.allAssetsLoaded = function() {
        var managers = this.managers;

        return managers.textureManager.getNumPendingTextures() === 0 &&
               managers.fontManager.getNumPendingFonts() === 0 &&
               managers.shaderManager.getNumPendingShaders() === 0;
    };

    Greebles.prototype.update = function() {
        var graphicsDevice = this.devices.graphicsDevice,
            clearColor = this.gameSettings.screen.clearColor,
            draw2d = this.draw2d,
            sprites = this.sprites,
            shaders = this.shaders,
            fpsElement = this.debug.fps.domElement;

        this.game.update();

        if (!graphicsDevice.beginFrame()) {
            return;
        }

        graphicsDevice.clear(clearColor, 1.0, 0.0);

        this.adjustView();

        // draw2d.begin('opaque', 'texture');
        // draw2d.drawSprite(sprites["BORDER_TOP"]);
        // draw2d.drawSprite(sprites["BORDER_LEFT"]);
        // draw2d.drawSprite(sprites["BORDER_RIGHT"]);
        // draw2d.drawSprite(sprites["BORDER_BOTTOM"]);
        // draw2d.end();

        this.game.render();

        graphicsDevice.endFrame();

        if (fpsElement) {
            this.displayPerformance();
        }
    };

    Greebles.prototype.shutdown = function() {
        var gameSession = this.gameSession,
            inputDevice = this.devices.inputDevice,
            fontManager = this.managers.fontManager,
            multiplayerSessionManager = this.managers.multiplayerSessionManager;

        if (fontManager) {
            fontManager.destroy();
        }

        if (multiplayerSessionManager) {
            multiplayerSessionManager.destroy();
        }

        // inputDevice.removeAllEventListeners();

        if (gameSession) {
            gameSession.destroy();
        }
    };

    Greebles.prototype.adjustView = function() {
        var graphicsDevice = this.devices.graphicsDevice,
            x = 0,
            y = 0;

        graphicsDevice.setViewport(x, y, graphicsDevice.width, graphicsDevice.height);
        graphicsDevice.setScissor(x, y, graphicsDevice.width, graphicsDevice.height);
    };

    Greebles.prototype.displayPerformance = function() {
        var graphicsDevice = this.devices.graphicsDevice,
            fpsElement = this.debug.fps.domElement,
            lastFPS = this.debug.fps.last,
            nextUpdate = this.debug.fps.nextUpdate,
            currentTime = TurbulenzEngine.time;

        if (currentTime > nextUpdate) {
            nextUpdate = (currentTime + 0.1);

            var fpsText = (graphicsDevice.fps).toFixed(2) + " fps";

            if (fpsText !== lastFPS) {
                lastFPS = fpsText;
                fpsElement.innerHTML = fpsText;
            }
        }
    };

    // Greebles constructor function
    Greebles.create = function(params) {
        var app = new Greebles();

        return app;
    };

    return Greebles;

})();