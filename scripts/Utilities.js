/**
 * Utility Functions
 */

/*global console: false */

function error(msg) {
    console.log("Utilities.js: " + msg);
}

function pointInRect(pt, rect) {
    if (!pt || pt.x === undefined || pt.y === undefined) {
        error("Point missing x or y");
        return false;
    }

    if (!rect || rect.x === undefined || rect.y === undefined || 
        rect.width === undefined || rect.height === undefined) {
        error("Rect missing attribute");
        return false;
    }

    return pt.x > rect.x && pt.x < (rect.x + rect.width) &&
           pt.y > rect.y && pt.y < (rect.y + rect.height);
}
