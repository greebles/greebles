/**
 * GameStates.js
 * Contains definitions for all the game states.
 */

/*global console: false */
/*global prompt: false */
/*global confirm: false */
/*global pointInRect: false */
/*global Graphics: false */
/*global levels: false */

var SinglePlayerGameState = {

    enter: function() {

    },

    update: function() {

    },

    render: function() {
        var draw2d = this.context.apis.draw2d,
            sprites = this.context.sprites,
            graphicsDevice = this.context.devices.graphicsDevice,
            level = levels[this.context.gameSettings.currentLevel];

        draw2d.begin('alpha', 'texture');
        draw2d.drawSprite(sprites["PATTERNS"]);

        // sprites[textureNames.hover].setTextureRectangle([
        //     hover.x, hover.y, hover.x + hover.width, hover.y + hover.height
        // ]);
        // sprites[textureNames.hover].x = hover.x;
        // sprites[textureNames.hover].y = hover.y;
        // sprites[textureNames.hover].setWidth(hover.width);
        // sprites[textureNames.hover].setHeight(hover.height);
        // draw2d.drawSprite(sprites[textureNames.hover]);

        draw2d.end();
    },

    exit: function() {

    }
};

var idleState = "IDLE",
    hoverState = "HOVER",
    pressedState = "PRESSED";

var renderHelpText = function() {
    var graphicsDevice = this.context.devices.graphicsDevice,
        mathDevice = this.context.devices.mathDevice,
        helpFont = this.context.helpFont,
        helpFontShader = this.context.helpFontShader,
        text = this.data.text,
        textX = this.data.textX,
        textY = this.data.textY;

    if (text) {
        graphicsDevice.setTechnique(helpFontShader.fontTechnique);

        var ftp = graphicsDevice.createTechniqueParameters({
            clipSpace: mathDevice.v4BuildZero(),
            alphaRef: 0.01,
            color: mathDevice.v4BuildOne()
        });

        ftp.clipSpace = mathDevice.v4Build(2 / graphicsDevice.width, -2 / graphicsDevice.height, -1, 1,
            ftp.clipSpace);
        ftp.color = mathDevice.v4Build(0.0, 0.0, 0.0, 1.0);
        graphicsDevice.setTechniqueParameters(ftp);

        helpFont.drawTextRect(text, {
            rect: [textX, textY, 0, 0], // for left-align width and height are ignored
            scale: 0.5,
            spacing: 0,
            alignment: 0
        });
    }
};

var GenericMenuState = {

    enter: function() {
        var self = this;

        this.data.buttons.forEach(function(b) {
            var str = "",
                noop = function() {
                    console.log("noop");
                };

            b.state = idleState;
            b.hover = b.hover || noop;
            b.pressed = b.pressed || noop;

            if (b.hover.substring) {
                str = b.hover.slice();
                b.hover = function() {
                    self.data.text = str;
                };
            }

            if (b.pressed.substring) {
                str = b.pressed.slice();
                b.pressed = function() {
                    console.log("Clicked: " + str);
                };
            }
        });
    },

    update: function() {
        var data = this.data,
            choices = this.data.buttons,
            mouse = this.context.devices.mouse,
            mouseCodes = this.context.devices.mouse.mouseCodes,
            position = this.context.devices.mouse.position;

        choices.forEach(function(c) {
            if (pointInRect(position, c)) {
                if (c.state === idleState) {
                    c.state = hoverState;
                    c.hover();
                } else if (c.state === hoverState && mouse.buttonDown(mouseCodes.BUTTON_0)) {
                    c.state = pressedState;
                } else if (c.state === pressedState && mouse.buttonUp(mouseCodes.BUTTON_0)) {
                    c.state = hoverState;
                    c.pressed();
                }
            } else {
                c.state = idleState;
            }
        });
    },

    render: function() {
        var draw2d = this.context.draw2d,
            sprites = this.context.sprites,
            textureNames = this.data.textureNames,
            buttons = this.data.buttons,
            graphicsDevice = this.context.devices.graphicsDevice,
            mathDevice = this.context.devices.mathDevice,
            hover,
            pressed;

        buttons.forEach(function(c) {
            if (c.state === hoverState) {
                hover = c;
            } else if (c.state === pressedState) {
                pressed = c;
            }
        });

        draw2d.begin('alpha', 'texture');
        draw2d.drawSprite(sprites[textureNames.idle]);

        if (hover) {
            sprites[textureNames.hover].setTextureRectangle([
                hover.x, hover.y, hover.x + hover.width, hover.y + hover.height
            ]);
            sprites[textureNames.hover].x = hover.x;
            sprites[textureNames.hover].y = hover.y;
            sprites[textureNames.hover].setWidth(hover.width);
            sprites[textureNames.hover].setHeight(hover.height);
            draw2d.drawSprite(sprites[textureNames.hover]);
        } else if (pressed) {
            sprites[textureNames.pressed].setTextureRectangle([
                pressed.x, pressed.y, pressed.x + pressed.width, pressed.y + pressed.height
            ]);
            sprites[textureNames.pressed].x = pressed.x;
            sprites[textureNames.pressed].y = pressed.y;
            sprites[textureNames.pressed].setWidth(pressed.width);
            sprites[textureNames.pressed].setHeight(pressed.height);
            draw2d.drawSprite(sprites[textureNames.pressed]);
        } else if (this.data.text !== "") {
            this.data.text = "";
        }

        draw2d.end();
    },

    exit: function() {

    },

};

var MultiplayerWaitingState = {

    enter: function() {
        GenericMenuState.enter.bind(this)();

        if (!this.context.isHost) {
            this.context.hostName = prompt("Do you have a buddy whose game you're trying to join? If so, enter their name here and we'll locate that game for you. Otherwise, leave it blank and click OK.");
        }
    },

    update: GenericMenuState.update,
    render: GenericMenuState.render,
    exit: GenericMenuState.exit,

    cancelClicked: function() {
        this.context.game.ss.pop();
    },

    joinClicked: function() {
        console.log("Working on it....");
    }

};

var GameplayDescriptionState = {

    enter: GenericMenuState.enter,
    update: GenericMenuState.update,

    render: function() {
        GenericMenuState.render.bind(this)();
        renderHelpText.bind(this)();
    },

    exit: GenericMenuState.exit,

    doneClicked: function() {
        this.context.game.ss.pop();
    }
};

var ItemDescriptionState = {

    enter: GenericMenuState.enter,
    update: GenericMenuState.update,

    render: function() {
        GenericMenuState.render.bind(this)();
        renderHelpText.bind(this)();
    },

    exit: GenericMenuState.exit,

    moreClicked: function() {
        this.context.game.ss.push(GameplayDescriptionState);
    },

    doneClicked: function() {
        this.context.game.ss.pop();
    }

};

var MainMenuState = {

    enter: GenericMenuState.enter,
    update: GenericMenuState.update,
    render: GenericMenuState.render,
    exit: GenericMenuState.exit,

    playClicked: function() {
        this.context.game.ss.push(SinglePlayerGameState);
    },

    hostNetworkGameClicked: function() {
        this.context.isHost = true;
        this.context.game.ss.push(MultiplayerWaitingState);
    },

    joinNetworkGameClicked: function() {
        this.context.isHost = false;
        this.context.game.ss.push(MultiplayerWaitingState);  
    },

    instructionsClicked: function() {
        this.context.game.ss.push(ItemDescriptionState);
    },

    quitClicked: function() {
        if (confirm("Close window?")) {
            window.close();
        }
    }
};

