// TURBULENZ LIBRARIES
/*{{ javascript("jslib/draw2d.js") }}*/
/*{{ javascript("jslib/fontmanager.js") }}*/
/*{{ javascript("jslib/observer.js") }}*/
/*{{ javascript("jslib/requesthandler.js") }}*/
/*{{ javascript("jslib/services/gamesession.js") }}*/
/*{{ javascript("jslib/services/mappingtable.js") }}*/
/*{{ javascript("jslib/services/multiplayersession.js") }}*/
/*{{ javascript("jslib/services/multiplayersessionmanager.js") }}*/
/*{{ javascript("jslib/services/turbulenzbridge.js") }}*/
/*{{ javascript("jslib/services/turbulenzservices.js") }}*/
/*{{ javascript("jslib/shadermanager.js") }}*/
/*{{ javascript("jslib/texturemanager.js") }}*/
/*{{ javascript("jslib/utilities.js") }}*/

// GREEBLES SYSTEMS
/*{{ javascript("scripts/data/assets.js") }}*/
/*{{ javascript("scripts/data/levels.js") }}*/
/*{{ javascript("scripts/Utilities.js") }}*/
/*{{ javascript("scripts/GameStates.js") }}*/
/*{{ javascript("scripts/data/menus.js") }}*/
/*{{ javascript("scripts/Greebles.js") }}*/

/*global TurbulenzEngine: true */
/*global Greebles: false */
TurbulenzEngine.onload = function onloadFn() {

    var greebles = Greebles.create(null);

    TurbulenzEngine.onunload = function onUnloadFn() {
        greebles.shutdown();
    };

    greebles.init();
};