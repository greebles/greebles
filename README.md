Greebles!
=========

A classic game brought to the web.

## Development

Greebles is built using the [Turbulenz][1] game engine. It is managed with [Grunt][2].

#### Getting Started

To test locally, use the Turbulenz local server. This emulates the environment on the Turbulenz hub and enables various services to work locally.

Run `[path to Turbulenz SDK]/start_local.sh`. This will start the local server. Navigate to `localhost:8070` in a browser to see the dashboard. This dashboard is used to both launch local versions of the game for testing, as well as for deploying the game to the Turbulenz Hub.

#### Notable Grunt Tasks

* `grunt all` - Build all versions.

[1]: http://biz.turbulenz.com/
[2]: http://gruntjs.com/


