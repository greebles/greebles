/*global module:false*/
/*global console:false*/
module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            src: ['Gruntfile.js', 'scripts/*.js', 'scripts/data/*.js'],
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                browser: true,
                globals: {
                    require: true,
                    define: true,
                    requirejs: true,
                    describe: true,
                    expect: true,
                    it: true
                }
            }
        },

        watch: {
            files: '<% jshint.src %>',
            tasks: ['jshint']
        },

        shell: {
            debug_canvas: {
                options: {
                    stdout: true,
                    stderr: true
                },
                command: 'makehtml --mode canvas-debug -t templates -t . app.js -o Greebles.canvas.debug.html debug_template.html'
            },
            debug_plugin: {
                options: {
                    stdout: true,
                    stderr: true
                },
                command: 'makehtml --mode plugin-debug -t templates -t . app.js -o Greebles.debug.html debug_template.html'
            },
            release_canvas: {
                options: {
                    stdout: true,
                    stderr: true
                },
                command: [
                    'maketzjs --mode canvas -t templates -t . -o Greebles.canvas.js app.js',
                    'makehtml --mode canvas -t templates -t . -o Greebles.canvas.release.html --code Greebles.canvas.js app.js release_template.html'
                ].join('&&')
            },
            release_plugin: {
                options: {
                    stdout: true,
                    stderr: true
                },
                command: [
                    'maketzjs --mode plugin -t templates -t . -o Greebles.tzjs app.js',
                    'makehtml --mode plugin -t templates -t . -o Greebles.release.html --code Greebles.tzjs app.js release_template.html'
                ].join('&&')
            },
            font_json: {
                options: {
                    stdout: true,
                    stderr: true
                },
                command: [
                    'rm -f assets/font/*.json',
                    'bmfont2json -p img/ -i assets/font/hero.fnt -o assets/font/hero.fnt.json'
                ].join('&&')
            },
            font_shader: {
                options: {
                    stdout: true,
                    stderr: true
                },
                command: [
                    'rm -f assets/shader/*.json',
                    '~/Turbulenz/SDK/0.28.0/tools/cgfx2json/bin/release/cgfx2json -i assets/shader/font.cgfx -o assets/shader/font.cgfx.json'
                ].join('&&')
            },
            delete_mapping_table: {
                command: [
                    'rm -f mapping_table.json',
                    'rm -f staticmax/*'
                ].join('&&')
            }
        },
        hash: {
            options: {
                mapping: 'mapping_table.json', //mapping file so your server can serve the right files
                srcBasePath: 'assets/', // the base Path you want to remove from the `key` string in the mapping file
                destBasePath: 'staticmax/', // the base Path you want to remove from the `value` string in the mapping file
                flatten: false, // Set to true if you don't want to keep folder structure in the `key` value in the mapping file
                hashLength: 12, // hash length, the max value depends on your hash function
                hashFunction: function(source, encoding) { // default is md5
                    return require('crypto').createHash('sha1').update(source, encoding).digest('hex');
                }
            },
            img: {
                src: ['assets/img/*.png', 'assets/img/*.dds'], //all your js that needs a hash appended to it
                dest: 'staticmax/' //where the new files will be created
            },
            screen: {
                src: 'assets/screen/*.png',
                dest: 'staticmax/'
            },
            font: {
                src: 'assets/font/*.fnt.json',
                dest: 'staticmax/'
            },
            shader: {
                src: 'assets/shader/*.cgfx.json',
                dest: 'staticmax/'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-hash');

    grunt.registerTask('convert_hash_table', 'Convert the hash table to a Turbulenz mapping table', function() {
        var fs = require('fs');
        var hashContents = fs.readFileSync('mapping_table.json');
        var table = JSON.parse(hashContents);

        var newTable = {};
        Object.keys(table).forEach(function(key) {
            if (/.*\.json/.test(key)) {
                newTable[key.substr(0, key.length - 5)] = table[key];
            } else {
                newTable[key] = table[key];
            }
        });

        var out = {
            version: 1.0,
            urnmapping: newTable
        };

        fs.writeFileSync('mapping_table.json', JSON.stringify(out));
    });
    grunt.registerTask('mapping_table', ['shell:delete_mapping_table', 'shell:font_json', 'shell:font_shader', 'hash', 'convert_hash_table']);
    grunt.registerTask('canvas', ['shell:debug_canvas', 'shell:release_canvas']);
    grunt.registerTask('plugin', ['shell:debug_plugin', 'shell:release_plugin']);
    grunt.registerTask('all', ['jshint', 'mapping_table', 'canvas', 'plugin']);
    grunt.registerTask('default', 'all');

};